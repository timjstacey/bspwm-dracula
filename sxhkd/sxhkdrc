#
# wm independent hotkeys

# make sxhkd reload its configuration files:
super + Escape
	pkill -USR1 -x sxhkd

# rofi
super + space
	~/.config/rofi/launchers/type-2/launcher.sh

super + ctrl + shift + q
	~/.config/rofi/powermenu/type-1/powermenu.sh

super + ctrl + q
	betterlockscreen -l

# save screen shot to clipboard
super + ctrl + shift + 4
	scrot -s -f -o /tmp/image.png && xclip -selection clipboard -t image/png -i /tmp/image.png

# full page screenshot saved to folder
super + shift + 4
	scrot '%Y-%m-%d_$wx$h.png' -e 'mv $f ~/Pictures/Screenshots'

# record selected area and save to ~/Videos
super + shift + 5
	escrotum -sr Videos/'%M%S.webm'

#
# Media control
#
{XF86AudioMute,XF86AudioLowerVolume,XF86AudioRaiseVolume,XF86AudioPlay,XF86AudioPrev,XF86AudioNext}
	pamixer {--toggle-mute,--decrease 5,--increase 5,play-pause,next,previous}

#ctrl + alt + shift + (Left,Down,Right}
#   spotifyctl -q {previous,playpause,next}

#
# Launch applications
#

ctrl + alt + shift + Return
	kitty

ctrl + alt + shift + q
  run-or-raise 'class = "Slack"' slack

ctrl + alt + shift + w
  run-or-raise 'class = "Brave-browser"' brave

ctrl + alt + shift + e
  run-or-raise 'class = "code-oss"' code

ctrl + alt + shift + a
  run-or-raise 'class = "beekeeper-studio"' beekeeper-studio

ctrl + alt + shift + s
  run-or-raise 'class = "Studio 3T"' studio-3t

#
# bspwm hotkeys
#

# restart bspwm
super + alt + {r,q}
	bspc {wm -r,quit}

# close and kill
super + {w,q}
	bspc node -{c,k}

# alternate between the tiled and monocle layout
ctrl + alt + shift + super + Return
	bspc desktop -l next

#
# focus/swap
#

# focus the node in the given direction
ctrl + alt + shift + {_,super + }{h,j,k,l}
	bspc node -{f,s} {west,south,north,east}

# focus or send to the given desktop
ctrl + alt + shift + {_,super + }{1-9,0}
	bspc {desktop -f,node -d} '^{1-9,10}'

# focus the next/previous node in the same window
alt + {shift + Tab,Tab}
	bspc node -f {prev,next}.local

# alt tab switcher
super + Tab
	bspc node -f last.!hidden.window

#
# move/resize
#

# expand a window by moving one of its side outward
super + alt + {h,j,k,l}
	bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# contract a window by moving one of its side inward
super + alt + shift + {h,j,k,l}
	bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

# move a floating window
super + {Left,Down,Up,Right}
	bspc node -v {-20 0,0 20,0 -20,20 0}

# Hide/Unhide Window
ctrl + alt + shift + v
	~/.config/bspwm/winhide

# Float/Unfloat current window
ctrl + alt + shift + space
    bspc node focused -t \~floating
